<?php

namespace JumpGroup\AcfHandling;

class RegisterOptionsPage {

  public static function init() {
  
    add_action('admin_menu', function(){
      if(function_exists('acf_add_options_page')){
        acf_add_options_page(array(
          'page_title' 	=> 'Opzioni Generiche',
          'menu_title'	=> 'Opzioni',
          'menu_slug' 	=> 'theme-general-settings',
          'capability'	=> 'edit_posts',
          'redirect'		=> false
        ));
      }
    },99);
  
  }
}
