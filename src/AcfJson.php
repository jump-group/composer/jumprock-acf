<?php

namespace JumpGroup\AcfHandling;

class AcfJson {

  public static $acf_path = WP_CONTENT_DIR . '/acf-json';

  public static function init() {
    add_filter('acf/settings/save_json',  function ($path) {
      $path = self::$acf_path;
      if (!file_exists($path)) {
       wp_mkdir_p($path);
      }
      return $path;
    });

    add_filter('acf/settings/load_json', function ( $paths ) {
      // append path
      $paths[] = self::$acf_path;
      return $paths;
    });
  }
}