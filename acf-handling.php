<?php
/*
Plugin Name:  ACF Handling Configs
Plugin URI:   https://jumpgroup.it/
Description:  Adds Configs for ACF
Version:      1.0.0
Author:       Jump Group
License:      MIT License
*/

namespace JumpGroup\AcfHandling;
use JumpGroup\AcfHandling\RegisterOptionsPage;
use JumpGroup\AcfHandling\AcfJson;

if ( ! defined( 'WPINC' ) ) {
	die;
}

class Init{

  protected static $instance;

    public static function get_instance(){
      if( null == self::$instance ){
        self::$instance = new self();
      }
      return self::$instance;
    }

    protected function __construct(){
      RegisterOptionsPage::init();
      AcfJson::init();
    }
}


$instance = Init::get_instance();